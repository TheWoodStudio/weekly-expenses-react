export const validateBudget = (budget) => {
    let totalBudget = parseInt(budget, 10) || 0;

    if (totalBudget > 0) {
        return totalBudget;
    } else {
        return false;
    }
}

export const reviewBudget = (budget, remaining) => {
    let cssClass;

    if ((budget / 4) > remaining) {
        cssClass = 'remaining red';
    } else if ((budget / 2) > remaining) {
        cssClass = 'remaining orange';
    } else {
        cssClass = 'remaining green';
    }

    return cssClass;
}