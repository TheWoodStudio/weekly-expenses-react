import React, { Component } from 'react';
import Expense from './Expense';
import PropTypes from 'prop-types';

class List extends Component {
    render() {
        return (
            <div className="expense-list">
                <h2>Expense List</h2>
                <ul>
                    {Object.keys(this.props.expenses).map(key => (
                        <Expense
                            key={key}
                            expense={this.props.expenses[key]}
                        />
                    ))}
                </ul>
            </div>
        );
    }
}

List.propTypes = {
    expenses: PropTypes.object.isRequired
}

export default List;