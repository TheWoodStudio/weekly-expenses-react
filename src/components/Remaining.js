import React, { Component } from 'react';
import { reviewBudget } from './../helperFunctions';
import PropTypes from 'prop-types';

class Remaining extends Component {
    render() {
        const budget = this.props.budget;
        const remaining = this.props.remaining;

        return (
            <p className={reviewBudget(budget, remaining)}>Remaining budget <span>${this.props.remaining}</span></p>
        );
    }
}

Remaining.propTypes = {
    budget: PropTypes.string.isRequired,
    remaining: PropTypes.string.isRequired
}

export default Remaining;