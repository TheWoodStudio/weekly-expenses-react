import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Budget extends Component {
    render() {
        return (
            <p className="budget">Initial budget <span>${this.props.budget}</span></p>
        );
    }
}

Budget.propTypes = {
    budget: PropTypes.string.isRequired 
}

export default Budget;