import React, { Component } from 'react';
import Header from './Header';
import Form from './Form';
import List from './List';
import BudgetBlock from './BudgetBlock';
import { validateBudget  } from './../helperFunctions';

class App extends Component {

  //Create state
  state = {
    budget: '',
    remaining: '',
    expenses: {}
  }

  componentDidMount() {
    this.getBudget();
  }
  
  getBudget = () => {
    let budget = prompt('How much do you have available?');
    let result = validateBudget(budget);

    if (result) {
      this.setState({
        budget: budget,
        remaining: budget
      });
    } else {
      this.getBudget();
    }
  }

  //Add new expense to state
  addExpense = (expense) => {
    //Actual state
    const expenses = {...this.state.expenses};

    //Add expense to state object
    expenses[`expense${Date.now()}`] = expense;

    //Extract
    this.extractBudget(expense.expenseAmount);
    //Add to state
    this.setState({
      expenses
    });
  }

  //Extract expense from remaining budget
  extractBudget = (amount) => {
    //Read expense
    let extract = Number(amount);

    //Get state copy
    let remaining = this.state.remaining;

    //Extract
    remaining -= extract;

    remaining = String(remaining);

    //Set new state
    this.setState({
      remaining: remaining
    });
  }

  render() {
    return (
      <div>
        <Header
          titulo="Weekly Expenses"
        />
        <div className="container">
          <div className="row">
            <div className="column">
              <Form
                addExpense = {this.addExpense}
              />
            </div>
            <div className="column">
              <List
                expenses={this.state.expenses}
              />
              <BudgetBlock
                budget={this.state.budget}
                remaining={this.state.remaining}
              />
            </div>
          </div>
        </div>
      </div>
      );
  }
}

export default App;
