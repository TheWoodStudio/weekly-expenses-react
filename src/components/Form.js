import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Form extends Component {

    //Form refs
    expenseName = React.createRef();
    expenseAmount = React.createRef();

    addExpense = (e) => {
        e.preventDefault();

        //Create object with data
        const expense = {
            expenseName: this.expenseName.current.value,
            expenseAmount: this.expenseAmount.current.value
        }

        //Add it and sent it via Props
        this.props.addExpense(expense);

        //Reset form
        e.currentTarget.reset();
    }

    render() {
        return (
            <form onSubmit={this.addExpense}>
                <h2>Add expenses</h2>
                <div className="field">
                    <label>Expense name</label>
                    <input type="text" placeholder="Transport, food, etc" ref={this.expenseName}/>
                </div>
                <div className="field">
                    <label>Amount</label>
                    <input type="number" placeholder="100, 300, 500" ref={this.expenseAmount}/>
                </div>
                <button>Add expense</button>
            </form>
        );
    }
}

Form.propTypes = {
    addExpense: PropTypes.func.isRequired
}

export default Form;