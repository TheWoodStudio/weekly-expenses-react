import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Expense extends Component {
    render() {
        const { expenseName, expenseAmount } = this.props.expense;

        return (
            <li>
                {expenseName}<span>${expenseAmount}</span>
            </li>
        );
    }
}

Expense.propTypes = {
    expense: PropTypes.object.isRequired
}

export default Expense;